<?php

// var_dump($_GET);

// var_dump($_POST);

// var_dump($_FILES);exit;


$data = current($_FILES);

foreach ($data['name'] as $k => $v) {

	if($data['error'][$k]>0){
		switch ($data['error'][$k]) {
			case '1':
				exit('上传的文件超过了 php.ini 中 upload_max_filesize 选项限制的值');
				break;
			case '2':
				exit('上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值。');
				break;
			case '3':
				exit('文件只有部分被上传');
				break;
			case '4':
				exit('没有文件被上传');
				break;
			default:
				break;
		}
	}

	$files = pathinfo($data['name'][$k]);

	$allowArr= ['png','jpg','gif','rar'];

	if(!in_array($files['extension'],$allowArr)){
		exit('上传类型不允许，允许的类型是：'.implode(",",$allowArr));
	}


	if($data['size'][$k]>100000){
		exit('文件过在，只允许上传100k');
	}


	$name = uniqid().mt_rand(1000,9999).'.'.$files['extension'];


	move_uploaded_file($data['tmp_name'][$k],'./upload/'.$name);


}

header("Location:./form.php");