<?php

/**
 * 
 */
class Code
{

	public $configs =[
		'font-size'=>30,
		'length'=>4,
		'width'=>200,
		'heigth'=>100,
		'isnotic'=>true
	];

	public function __construct($configs=[])
	{
		$this->configs = array_merge($this->configs,$configs);
	}
	
	public function entry()
	{
		var_dump($this->configs);
	}
}