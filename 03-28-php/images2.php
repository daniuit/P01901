<?php

header("Content-type: image/jpeg");

$meinv = imageCreateFromjpeg('./meinv.jpg');

$w = imagesx($meinv);
$h = imagesy($meinv);

$max = imageCreateTrueColor($w/2,$h/2);
$mid = imageCreateTrueColor($w/4,$h/4);
$min = imageCreateTrueColor($w/8,$h/8);

imagecopyresized ($max , $meinv, 0, 0, 0 , 0 ,$w/2, $h/2 ,$w ,$h);

imagecopyresized ($mid , $meinv, 0, 0, 0 , 0 ,$w/4, $h/4 ,$w ,$h);

imagecopyresized ($min , $meinv, 0, 0, 0 , 0 ,$w/8, $h/8 ,$w ,$h);

imagejpeg($max,'./max_'.uniqid().'.jpg');
imagejpeg($mid,'./mid_'.uniqid().'.jpg');
imagejpeg($min,'./min_'.uniqid().'.jpg');




