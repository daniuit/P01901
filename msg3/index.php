<?php
// 入口文件
include './functions.php';
include './Msg.php';
include './User.php';
include './Db.php';


session_start();

$action = isset($_GET['a'])?$_GET['a']:'show';

$class = isset($_GET['c'])?ucfirst($_GET['c']):'Msg';

$msg = new $class();

$msg->$action();
