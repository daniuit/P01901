<?php
// 1  封装一个函数，随机产生指定长度的字符

function rand_str_get($num=7)
{
	$str ="qwertyuiopasdfghjklzxcvbnmWERTYUIOPASDFGHJKLZXCVBNM1234567890";

	$temp = "";

	for ($i=0; $i <$num ; $i++) { 
		$index=mt_rand(0,strlen($str)-1);
		$temp .= $str[$index];
	}

	return $temp;
}

function change_str($str){

	$temp = '';
	for ($i=0; $i < strlen($str) ; $i++) { 
		
		if($str[$i]==strtoupper($str[$i])){
			$temp .= strtolower($str[$i]);
		}else{
			$temp .= strtoupper($str[$i]);
		}
	}

	return $temp;
}
function array_son_long_get($arr)
{
	$max = 0;
	$index = 0;
	foreach ($arr as $k => $v) {
		if(is_array($v)){
			if(count($v)>$max){
				$max = count($v);
				$index = $k;
			}
		}
	}
	return $arr[$index];	
}


function change_str_format($str,$arr=[2,6],$fuhao='-'){
	$temp = "";

	for ($i=0; $i <strlen($str) ; $i++) { 
		if(in_array($i,$arr)){
			$temp .= $str[$i].$fuhao;
		}else{
			$temp .= $str[$i];
		}
	}
	return $temp;
}


function change_str_format1($str)
{
	$arr = explode('-',$str);

	$temp = '';

	foreach ($arr as $k => $v) {
		if($k>0){
			$temp .= ucfirst($v);
		}else{
			$temp .=$v;
		}
	}
	return $temp;
}

function size_format($byte){

	if($byte<1024){
		return $byte."b";
	}elseif ($byte<pow(1024,2)) {
		return round($byte/pow(1024,1),2).'k';
	}elseif ($byte<pow(1024,3)) {
		return round($byte/pow(1024,2),2).'m';
	}elseif ($byte<pow(1024,4)) {
		return round($byte/pow(1024,3),2).'g';
	}else{
		return round($byte/pow(1024,4),2).'t';
	}
}

function date_format1($time)
{
	$diff = time()-$time;

	if($diff<60){
		return "刚刚";
	}elseif($diff<3600){
		return floor($diff/60)."分钟之前";
	}elseif($diff<86400){
		return floor($diff/3600)."小时之前";
	}elseif($diff<(86400*7)){
		return floor($diff/86400)."天之前";
	}else{
		return date("Y-m-d H:i:s",$time);
	}
	
}

function is_image($filename){

	$image_type = ['jpg','png','gif','bmp','jpeg'];

	$arr = explode('.',$filename);

	$suffix = end($arr);

	if(in_array($suffix,$image_type)){
		return true;
	}else{
		return false;
	}
}


function age_sort($arr)
{
	$temp = [];

	foreach ($arr as $k => $v) {
		$temp[$v['age']][] =$v;
	}
	// var_dump($temp);
	ksort($temp);

	return $temp;
}

function del_dir($dir)
{
	$files = glob($dir."/*");

	foreach ($files as  $file) {
		if(is_dir($file)){
			del_dir($file);
		}else{
			unlink($file);
			// echo "unlink:".$file.'<br>';
		}
	}
	// echo "remdir:".$dir.'<br>';
	remdir($dir);
}

function config($key)
{
	$configs = include './config.php';

	if(isset($configs[$key])){
		return $configs[$key];
	}else{
		return $configs;
	}
}

function time_format($time)
{
	$diff = time()-$time;

	if($diff<60){
		return "刚刚";
	}elseif($diff<3600){
		return floor($diff/60)."分钟前";
	}elseif($diff<86400){
		return floor($diff/3600)."小时前";
	}elseif($diff<(86400*7)){
		return floor($diff/86400)."天前";
	}else{
		return date("Y-m-d H:i:s",$time);
	}
	
}

?>