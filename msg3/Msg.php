<?php
/**
 * 
 */
class Msg 
{

	
	public function add($value='')
	{

		if(!isset($_SESSION['username'])){
			header('Location:./index.php');
			exit;
		}
		
		$data = $_POST;

		$data['username'] = $_SESSION['username'];
		$data['ctime'] = time();

		$db = new Db('msg');

		$db->add($data);

		header("Location:./index.php");
	}

	public function edit()
	{
		$id = $_GET['id'];

		$db = new Db('msg');

		$data = $db->find($id);

		include './template/edit.html';
	}

	public function del()
	{
		$id = $_GET['id'];

		$db = new Db('msg');

		$db->delete($id);

		header("Location:./index.php");

	}

	public function save()
	{
		$id = $_GET['id'];

		$data = $_POST;

		$data['username'] = $_SESSION['username'];

		$db = new Db('msg');

		$db->save($data,$id);

		header("Location:./index.php");

	}

	public function show()
	{
		
		$db = new Db('msg');

		$data = $db->findAll('0,3');

		include './template/index.html';

		var_dump($_SESSION);
	}

	public function more()
	{
		$page = $_POST['page'];

		$db = new Db('msg');

		$data = $db->findAll((($page-1)*3).',3');

		if(!$data){
			echo "<p >我是有底线的！！！</p>";
		}

		include './template/more.html';
	}


}