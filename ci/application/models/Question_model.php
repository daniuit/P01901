<?php

/**
 * @Author: xuebingsi
 * @Date:   2019-05-08 16:23:45
 * @Last Modified by:   zhibinm
 * @Last Modified time: 2019-05-08 16:26:25
 */
class Question_model extends CI_Model {

    public function get_top()
    {
        return $this->db->get_where('bbs_question',['is_top'=>'1'])->result_array();
    }

}