<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	public function index()
	{

		// $this->load->database();

		// $query = $this->db->query('select * from bbs_user');

		// var_dump($query->result_array());

		// $query = $this->db->get_where('bbs_user',['user_id'=>6])->result_array();

		// Db::name('bbs_user')->where('user_id',6)->find();
		// 
		// $this->db->select('*');
		// $this->db->from('bbs_question t1');
		// $this->db->join('bbs_user t2', 't1.user_id = t2.user_id');
		// $query = $this->db->get()->result_array();


		// $query = $this->db->get_where('bbs_question',['is_top'=>'1'])->result_array();
		
		// User::get(78);
		// User::all();
		// 
		$this->load->model('question_model');

		$query = $this->question_model->get_top();

		// var_dump($query);

		// exit;

		// var_dump($_GET);

		// echo "index-index";
		
		$this->load->view('header');
        $this->load->view('reg/index',['name'=>'学并思','question'=>$query]);
        $this->load->view('footer');
	}

	public function code()
	{
		$this->load->helper('captcha');

		$vals = array(
		    'word'      => 'rtrt',
		    'img_path'  => './captcha/',
		    'img_url'   => 'http://127.0.0.1/p201901/ci/captcha',
		    'font_path' => './static/BOOKPB.TTF',
		    'img_width' => '150',
		    'img_height'    => 30,
		    'expiration'    => 7200,
		    'word_length'   => 8,
		    'font_size' => 16,
		    'img_id'    => 'Imageid',
		    'pool'      => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

		    // White background and border, black text and red grid
		    'colors'    => array(
		        'background' => array(255, 255, 255),
		        'border' => array(255, 255, 255),
		        'text' => array(0, 0, 0),
		        'grid' => array(255, 40, 40)
		    )
		);

		$cap = create_captcha($vals);

		var_dump($cap);

		echo $cap['image'];
	}

	public function reg()
	{
		var_dump($_GET);
		echo "site/reg";
		$this->load->view('header');
        $this->load->view('reg');
        $this->load->view('footer');
	}
}
