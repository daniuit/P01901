<?php

// 1  封装一个函数，随机产生指定长度的字符

function rand_str_get($num=7)
{
	$str ="qwertyuiopasdfghjklzxcvbnmWERTYUIOPASDFGHJKLZXCVBNM1234567890";

	$temp = "";

	for ($i=0; $i <$num ; $i++) { 
		$index=mt_rand(0,strlen($str)-1);
		$temp .= $str[$index];
	}

	return $temp;
}

// $res = rand_str_get(32);

// var_dump($res);

// 2  "dfdfNdfdDSF"  实现大小写转换

function change_str($str){

	$temp = '';
	for ($i=0; $i < strlen($str) ; $i++) { 
		
		if($str[$i]==strtoupper($str[$i])){
			$temp .= strtolower($str[$i]);
		}else{
			$temp .= strtoupper($str[$i]);
		}
	}

	return $temp;
}

// var_dump('dfdfNdfdDSF');

// $res = change_str('dfdfNdfdDSF');

// var_dump($res);


// 3  $arr = [34,54,[45,2,2,2,2],45,[56,56,3,5,6,6,6,56],[56,53]]; 把数组中子数组长度最长找出来。

$arr = [34,54,[45,2,2,2,2],45,[56,56,3,5,6,6,6,56],[56,53]];
function array_son_long_get($arr)
{
	$max = 0;
	$index = 0;
	foreach ($arr as $k => $v) {
		if(is_array($v)){
			if(count($v)>$max){
				$max = count($v);
				$index = $k;
			}
		}
	}
	return $arr[$index];	
}

// $res = array_son_long_get($arr);

// var_dump($res);


// 4 "18925139194" 转换成 "189-2513-9194"

function change_str_format($str,$arr=[2,6],$fuhao='-'){
	$temp = "";

	for ($i=0; $i <strlen($str) ; $i++) { 
		if(in_array($i,$arr)){
			$temp .= $str[$i].$fuhao;
		}else{
			$temp .= $str[$i];
		}
	}
	return $temp;
}

$res = change_str_format("18925139194",[3,9],'*');

var_dump($res);




// 5 "get-element-by-id" 转成蛇峰写法 "getElementById";

function change_str_format1($str)
{
	$arr = explode('-',$str);

	$temp = '';

	foreach ($arr as $k => $v) {
		if($k>0){
			$temp .= ucfirst($v);
		}else{
			$temp .=$v;
		}
	}
	return $temp;
}

$res = change_str_format1("get-element-by-id");
// $res = change_str_format1("background-color");

// var_dump($res);

// 6 封装一个函数，实现单位的转换 a(34534534534); 34k/45m/45G

function size_format($byte){

	if($byte<1024){
		return $byte."b";
	}elseif ($byte<pow(1024,2)) {
		return round($byte/pow(1024,1),2).'k';
	}elseif ($byte<pow(1024,3)) {
		return round($byte/pow(1024,2),2).'m';
	}elseif ($byte<pow(1024,4)) {
		return round($byte/pow(1024,3),2).'g';
	}else{
		return round($byte/pow(1024,4),2).'t';
	}
}
$res = size_format(34534534534);

var_dump($res);

// 7 封装一个函数，实现判断输入的时间与现在时间有差，如果是一分钟之内返回刚刚，一小时之内返回几分钟前，一天之内返回几小时前，7天之内返回几天前，超过7天，返回具体的日期

function date_format1($time)
{
	$diff = time()-$time;

	if($diff<60){
		return "刚刚";
	}elseif($diff<3600){
		return floor($diff/60)."分钟之前";
	}elseif($diff<86400){
		return floor($diff/3600)."小时之前";
	}elseif($diff<(86400*7)){
		return floor($diff/86400)."天之前";
	}else{
		return date("Y-m-d H:i:s",$time);
	}
	
}
$res = date_format1(time()-1270000);

var_dump($res);
// 8 判断是一个文件是否 图片格式 "34534.jpg"

function is_image($filename){

	$image_type = ['jpg','png','gif','bmp','jpeg'];

	$arr = explode('.',$filename);

	$suffix = end($arr);

	if(in_array($suffix,$image_type)){
		return true;
	}else{
		return false;
	}
}
$res = is_image('t45.45.435.png.txt');

// var_dump($res);
// 9 $arr = [
// 	['name'=>'小明','age'=>'18'],
// 	['name'=>'小红','age'=>'20'],
// 	['name'=>'小花','age'=>'30'],
// ]; 按年龄进行排序。
$arr = [
	['name'=>'小明','age'=>'18'],
	['name'=>'小红','age'=>'20'],
	['name'=>'小花3','age'=>'30'],
	['name'=>'小花3','age'=>'60'],
	['name'=>'小花2','age'=>'70'],
	['name'=>'小花1','age'=>'70'],
];

function age_sort($arr)
{
	$temp = [];

	foreach ($arr as $k => $v) {
		$temp[$v['age']][] =$v;
	}
	// var_dump($temp);
	ksort($temp);

	return $temp;
}

$res = age_sort($arr);

var_dump($res);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<table>
		<tr>
			<td>姓名</td>
			<td>年龄</td>
		</tr>
		<?php foreach ($res as $v) {
			foreach ($v as $vv){ ?>
				<tr>
					<td><?php echo $vv['name']; ?></td>
					<td><?php echo $vv['age']; ?></td>
				</tr>	
			
		<?php } } ?>
	</table>
</body>
</html>