/*
* @Author: xuebingsi
* @Date:   2019-04-04 09:47:48
* @Last Modified by:   zhibinm
* @Last Modified time: 2019-04-04 10:15:53
*/
社区的库

3张表

用户
主键
昵称
密码
手机
头像
邮箱
性别
等级
城市
签名
状态
注册时间
登录时间
第三方平台
飞吻

create table user(
	user_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	nickname char(30) unique  not null COMMENT "用户名",
	password char(32) not null COMMENT "密码",
	phone char(32) not null COMMENT "手机",
	face char(100) not null COMMENT "头像",
	level TINYINT not null COMMENT "等级",
	sign char(200) not null COMMENT "签名",
	email char(50) not null COMMENT "邮箱",
	sex enum('男','女','保密') default "保密" COMMENT "性别",
	city char(20) default '' COMMENT "城市",
	status enum('0','1','2') default '0' COMMENT "状态 0 未审核 1 审核 2 禁用",
	ctime int(10) unsigned COMMENT "创建时间",
	ltime int(10) unsigned COMMENT "登录时间",
	kiss int(10) unsigned default 0 COMMENT "飞吻"
);

问题
主键
标题
内容
所属分类
点赞数量
回复数量
收藏数量
问题创建时间
问题状态
是否置顶
是否精华
问题所属用户


create table question(
	qestion_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	title char(30)   not null COMMENT "标题",
	content text  not null COMMENT "内容",
	cate_id int(10) unsigned not null COMMENT "分类",
	user_id int(10) unsigned not null COMMENT "用户id",
	view_num int(10)  unsigned not null default 0 COMMENT "浏览数量",
	reply_num int(10) unsigned not null default 0 COMMENT "回复数量",
	status enum('0','1','2') default '0' COMMENT "状态 0 未结 1 已结",
	is_top enum('0','1') default '0' COMMENT "0 正常 1 顶置",
	is_good enum('0','1') default '0' COMMENT "0 正常 1 精华"，
	ctime int(10) unsigned COMMENT "创建时间"
);



回复表
主键
内容
所属问题
所属用户
点赞数量
回复时间
回复状态

create table reply(
	reply_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	content text  not null COMMENT "内容",
	question_id int(10) unsigned not null COMMENT "问题id",
	user_id int(10) unsigned not null COMMENT "用户id",
	ok_num int(10)  unsigned not null default 0 COMMENT "点赞数量",
	ctime int(10) unsigned COMMENT "创建时间",
	status enum('0','1') default '0' COMMENT "状态 0 普通回复 1 最佳回复"
);


点赞表

create table zan(
	zan_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	reply_id int(10) unsigned not null COMMENT "回复id",
	user_id int(10) unsigned not null COMMENT "用户id",
	ctime int(10) unsigned COMMENT "创建时间"
);

收藏表

create table collect(
	collect_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	question_id int(10) unsigned not null COMMENT "问题id",
	user_id int(10) unsigned not null COMMENT "用户id",
	ctime int(10) unsigned COMMENT "创建时间"
);

消息表

create table msg(
	msg_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	question_id int(10) unsigned not null COMMENT "问题id",
	dst_user_id int(10) unsigned not null COMMENT "目标用户id",
	src_user_id int(10) unsigned not null COMMENT "源用户id",
	type enum('0','1','2') default '0' COMMENT "0 点赞 1 回复 2 收藏",
	ctime int(10) unsigned COMMENT "创建时间"
);

链接表

create table link(
	link_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	link_name int(10) unsigned not null COMMENT "链接名",
	link_url char(30) unsigned not null COMMENT "url",
	is_show status('0','1') default '0' COMMENT "是否显示"
);


分类表

create table cate(
	cate_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	cate_name int(10) unsigned not null COMMENT "问题id",
	orderby int(10) unsigned not null COMMENT "排序",
	ctime int(10) unsigned COMMENT "创建时间"
);


广告表

create table ad(
	ad_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	ad_name int(10) unsigned not null COMMENT "广告名",
	ad_url char(30) unsigned not null COMMENT "url",
	ad_imgage char(30) unsigned not null COMMENT "图片",
	is_show enum('0','1') default '0' COMMENT "是否显示"
);

签到表

create table sign(
	sign_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	user_id int(10) unsigned not null COMMENT "用户id",
	sign_num int(10) unsigned not null COMMENT "签到数量",
	ctime int(10) unsigned COMMENT "创建时间"
);
