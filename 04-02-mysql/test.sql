/*
* @Author: xuebingsi
* @Date:   2019-04-02 09:53:10
* @Last Modified by:   zhibinm
* @Last Modified time: 2019-04-08 11:14:57
*/
社区的库

3张表

用户
主键
用户名
密码
手机
邮箱
性别
城市
状态
注册时间
登录时间
积分

create table user(
	user_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	username char(30) unique  not null COMMENT "用户名",
	password char(32) not null COMMENT "密码",
	phone char(32) not null COMMENT "手机",
	email char(50) not null COMMENT "邮箱",
	sex enum('男','女','保密') default "保密" COMMENT "性别",
	city char(20) default '' COMMENT "城市",
	status enum('0','1','2') default '0' COMMENT "状态 0 未审核 1 审核 2 禁用",
	ctime int(10) unsigned COMMENT "创建时间",
	ltime int(10) unsigned COMMENT "登录时间",
	sorce int(10) unsigned default 0 COMMENT "积分"
);

问题
主键
标题
内容
所属分类
点赞数量
回复数量
收藏数量
问题创建时间
问题状态
是否置顶
是否精华
问题所属用户


create table question(
	qestion_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	title char(30)   not null COMMENT "标题",
	content text  not null COMMENT "内容",
	cate_id int(10) unsigned not null COMMENT "分类",
	user_id int(10) unsigned not null COMMENT "用户id",
	ok_num int(10)  unsigned not null default 0 COMMENT "点赞数量",
	reply_num int(10) unsigned not null default 0 COMMENT "回复数量",
	collect_num int(10) unsigned not null default 0 COMMENT "收藏数量",
	ctime int(10) unsigned COMMENT "创建时间",
	status enum('0','1','2') default '0' COMMENT "状态 0 未审核 1 审核 2 禁用",
	is_top enum('0','1') default '0' COMMENT "0 正常 1 顶置",
	is_good enum('0','1') default '0' COMMENT "0 正常 1 精华"
);



回复表
主键
内容
所属问题
所属用户
点赞数量
回复时间
回复状态

create table reply(
	reply_id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	content text  not null COMMENT "内容",
	question_id int(10) unsigned not null COMMENT "问题id",
	user_id int(10) unsigned not null COMMENT "用户id",
	ok_num int(10)  unsigned not null default 0 COMMENT "点赞数量",
	ctime int(10) unsigned COMMENT "创建时间",
	status enum('0','1') default '0' COMMENT "状态 0 普通回复 1 最佳回复"
);
