

select * from user;

select name,sex,age,class,id from user;

select name,sex s from user;

select name,sex as s from user;

select * from user where (sex='男' or age<=20) and class="PHP";

-- > < >= <=  <>  
select DISTINCT class from user;

select * from user order by age
select * from user order by age asc;
select * from user order by age desc;

select * from user where sex='男' order by age desc;

select * from user order by age desc limit 10;

select * from user order by age desc limit 10,10;

select * from user order by age desc limit 20,10;

select * from user order by age desc limit 30,10;

select * from user order by age desc limit 3,3;


select * from user where age BETWEEN 20 and 30;
-- 等同===
select * from user where age>=20 and age<=30;


select * from user where id=2 or id=6 or id=7 or id=9;
-- 等同===
select * from user where id in (2,4,7,9);



select * from user where name like "%明%";

select * from user where name like "明%";

select * from user where name like "%明";

select * from user where name like "明";
-- 等同===
select * from user where name = "明";


select name,if(age>25,'大叔','少年') from user;

select * from user order by rand() limit 3;

-- 子查询 


alter table users rename user;

alter table user change email mail char(30) default '000';

alter table user modify mail char(50);

alter table user add city char(20) after name;

alter table user drop city;

ALTER TABLE user ADD PRIMARY KEY (id);

ALTER TABLE user DROP PRIMARY KEY;


select count(*) cn from user;

select count(*) cn from user where sex="男";

select max(age) from user;

select min(age) from user;

select * from user where age =(select max(age) from user);

select count(*),sex cn from user group by sex;

select avg(age),class,sex from user group by class,sex;

