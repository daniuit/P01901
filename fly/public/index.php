<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
define('WEIXIN_APP_ID', 'WEIXIN_APP_ID');
define("CAPTCHA_ID", "680270de845a61a57208df732bcc9e80");
define("PRIVATE_KEY", "b6e9bbd62654b461e6b9adb9e2e10de9");
// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';
