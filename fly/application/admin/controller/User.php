<?php
namespace app\admin\controller;

use app\index\model\User as UserModel;
use think\Controller;

class User extends Common
{
    public function memberlist()
    {
    	$where = [];


    	if(input('start') && input('end')){

    		$where['create_time'] = [
    			['>=',strtotime(input('start'))],
    			['<=',strtotime(input('end'))],
    		];
    	}elseif(input('start')){
    		$where['create_time'] = [
    			['>=',strtotime(input('start'))],
    			['<=',time()],
    		];
    	}elseif(input('end')){
    		$where['create_time'] = [
    			['>=','0'],
    			['<=',strtotime(input('end'))],
    		];
    	}

    	if(input('nickname')){

    		$where['nickname'] = ['like',"%".input('nickname')."%"];
    	}

    	// var_dump($where);exit;

    	$data = UserModel::where($where)->paginate(3);

    	$this->assign('data',$data);
    	return $this->fetch();
    }

    public function memberlist1()
    {
    	return $this->fetch();
    }

    public function ajax_list()
    {
    	$where = [];

    	if(input('start') && input('end')){

    		$where['create_time'] = [
    			['>=',strtotime(input('start'))],
    			['<=',strtotime(input('end'))],
    		];
    	}elseif(input('start')){
    		$where['create_time'] = [
    			['>=',strtotime(input('start'))],
    			['<=',time()],
    		];
    	}elseif(input('end')){
    		$where['create_time'] = [
    			['>=','0'],
    			['<=',strtotime(input('end'))],
    		];
    	}

    	if(input('nickname')){

    		$where['nickname'] = ['like',"%".input('nickname')."%"];
    	}

    	$data = UserModel::where($where)->paginate(input('limit'));

		exit(json_encode(['code'=>0,'message'=>'ok','count'=>$data->total(),'data'=>$data->items()]));
    }

    public function welcome()
    {
    	return $this->fetch();
    }

    public function stop()
    {
    	$user_id = input('user_id');

    	$user = UserModel::get($user_id);

    	if($user->status=='正常'){
    		$user->status = '1';
    	}else{
    		$user->status = '0';
    	}
    	
    	$user->save();

    	$this->success("修改成功");
    }

    public function edit()
    {

    	$user_id = input('user_id');

    	$user =UserModel::get($user_id);

    	$this->assign('user',$user);

    	return $this->fetch();
    }

}
