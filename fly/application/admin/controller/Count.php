<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;

class Count extends Common
{
    public function User()
    {
    	return $this->fetch();
    }

    public function jie()
    {
    	return $this->fetch();
    }

    public function get_user()
    {
    	$data = Db::query("select count(*) num,FROM_UNIXTIME(create_time,'%Y-%m-%d') date from bbs_user where create_time>=? GROUP BY date",[strtotime(date("Y-m-d",strtotime('-30 days')))]);
    	$this->success('ok',null,$data);
    }

    public function get_jie()
    {
    	$data = Db::query("select count(*) num,FROM_UNIXTIME(create_time,'%Y-%m-%d') date from bbs_question where create_time>=? GROUP BY date",[strtotime(date("Y-m-d",strtotime('-30 days')))]);
    	$this->success('ok',null,$data);
    }

}
