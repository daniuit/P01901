<?php
namespace app\admin\controller;

use app\admin\model\AuthRole;
use think\Controller;
use think\Db;

class Role extends Common
{
    public function role_list()
    {
    	$roles = AuthRole::all();
    	$this->assign('roles',$roles);
    	return $this->fetch();
    }

    public function add()
    {
    	$rules = Db::name('auth_rule')
    	->alias('t1')
    	->join('auth_cate t2','t1.cate_id=t2.cate_id')
    	->select();

    	$temp = [];

    	foreach ($rules as $k => $rule) {
    		$temp[$rule['cate_name']][] = $rule;
    	}
    	$this->assign('data',$temp);
    	return $this->fetch();
    }

    public function edit()
    {
    	$id = input('id');

    	$role = AuthRole::get($id);

    	$role->rule_arr = explode(',',$role->rule_id);

    	$this->assign('role',$role);

    	$rules = Db::name('auth_rule')
    	->alias('t1')
    	->join('auth_cate t2','t1.cate_id=t2.cate_id')
    	->select();

    	$temp = [];

    	foreach ($rules as $k => $rule) {
    		$temp[$rule['cate_name']][] = $rule;
    	}
    	$this->assign('data',$temp);

    	return $this->fetch();
    }

    public function create()
    {
    	$data = input('post.');


    	// todo::验证数据

    	$data['rule_id'] = implode(',',$data['id']);

    	$res = AuthRole::create($data);

    	if($res){
    		$this->success('增加成功');
    	}else{
    		$this->error('增加失败');
    	}
    }

}
