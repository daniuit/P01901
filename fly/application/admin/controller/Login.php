<?php
namespace app\admin\controller;

use app\admin\model\Admin;
use think\Controller;

class Login extends Controller
{
    public function index()
    {
        return $this->fetch();
    }

    public function out()
    {
    	session(null);

    	$this->success("退出成功",url('admin/login/index'));
    }

    public function check()
    {
    	$data = input('post.');

		$admin = Admin::get(['nickname'=>$data['nickname']]);

		if($admin && $admin->password==md5($data['password'])){

			session('admin_id',$admin->admin_id);
			session('nickname',$admin->nickname);
			$this->success('登录成功');
		}else{
			$this->error('帐号密码不正确Q！！！');
		}
    }

}

