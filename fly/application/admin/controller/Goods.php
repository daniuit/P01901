<?php
namespace app\admin\controller;

use app\index\model\Brand;
use app\index\model\Goods as GoodsModel;
use app\index\model\GoodsSpecs;
use app\index\model\GoodsSpecsList;
use app\index\model\ShopCate;
use think\Controller;
use think\Db;

class Goods extends Common
{
    public function index()
    {
    	return $this->fetch();
    }

    public function ajax_list()
    {
    	$where = [];

    	if(input('start') && input('end')){

    		$where['create_time'] = [
    			['>=',strtotime(input('start'))],
    			['<=',strtotime(input('end'))],
    		];
    	}elseif(input('start')){
    		$where['create_time'] = [
    			['>=',strtotime(input('start'))],
    			['<=',time()],
    		];
    	}elseif(input('end')){
    		$where['create_time'] = [
    			['>=','0'],
    			['<=',strtotime(input('end'))],
    		];
    	}

    	if(input('nickname')){

    		$where['nickname'] = ['like',"%".input('nickname')."%"];
    	}

    	$data = GoodsModel::where($where)->order('goods_id desc')->paginate(input('limit'));

		exit(json_encode(['code'=>0,'message'=>'ok','count'=>$data->total(),'data'=>$data->items()]));
    }

    public function add()
    {
    	$brand = Brand::all();
    	$cates = ShopCate::where('fid','0')->select();

    	$this->assign('brand',$brand);
    	$this->assign('cates',$cates);

    	return $this->fetch();
    }

    public function get_cate()
    {
    	$cid = input('cate_id');

    	$cates = ShopCate::where('fid',$cid)->select();

    	$this->success('ok',null,$cates);
    }

    public function save()
    {
    	$data = input('post.');

    	// todo::验证数据
    	$data['shop_image_list'] = json_encode($data['shop_image_list']);

    	if(input('attr_title')){
    		$temp = [];

	    	foreach ($data['attr_title'] as $k => $v) {
	    		$temp[$k]['title'] = $v;
	    		$temp[$k]['value'] = $data['attr_values'][$k];
	    	}

	    	$data['shop_attr'] = json_encode($temp);
    	}

    
    	$res = GoodsModel::create($data);

    	if($res){
    		$this->success('ok');
    	}else{
    		$this->error('error');
    	}
    }

    public function sku()
    {
    	$goods_id = input('goods_id');
    	$specs = GoodsSpecs::where('goods_id',$goods_id)->select();

    	$temp = [];

    	foreach ($specs as $k => $v) {
    		$temp[$v['specs_type']][] = $v['specs_value'];
    	}

    	$this->assign('goods_id',$goods_id);
    	$this->assign('specs',$temp);
    	return $this->fetch();
    }

    public function add_sku()
    {
    	$data = input('post.');

    	foreach ($data['spece_title'] as $k=> $v) {
    		
    		$arr = explode(',',$data['spece_values'][$k]);

    		foreach ($arr as $vv) {

    			$insertData = [
    				'goods_id'=>$data['goods_id'],
    				'specs_type'=>$v,
    				'specs_value'=>$vv,
    			];

    			GoodsSpecs::create($insertData);
    		}
    	}

    	// var_dump($data);
    }

    public function specs_list()
    {
    	$goods_id = input('goods_id');

    	$specs = Db::name('goods_specs')->order('specs_id')->where('goods_id',$goods_id)->select();

        $temp1 = [];

        $temp5 = [];

        foreach ($specs as $k => $v) {
            $temp1[$v['specs_type']][] = $v['specs_id'];
            $temp5[$v['specs_id']] = $v;
        }

        $temp6 = array_keys($temp1);

        $temp2 = array_values($temp1);

        $temp3 = di_ka_er_ji($temp2);

        $temp4 = [];

        foreach ($temp3 as  $v) {
            $temp4[$v] = explode('|',$v);
        }

        $specs_list = Db::name('goods_specs_list')->where('goods_id',$goods_id)->select();

        $temp7 = [];

        foreach ($specs_list as $k => $v) {
            $temp7[$v['specs_id']] = $v;
        }

        $this->assign('temp4',$temp4);
        $this->assign('temp5',$temp5);
        $this->assign('temp7',$temp7);
        $this->assign('temp6',$temp6);
        $this->assign('goods_id',$goods_id);
    	return $this->fetch();
    }


    public function add_sku_list()
    {
    	$data = input('post.');


        foreach ($data['specs_price'] as $k => $v) {

            $insertData['goods_id'] = $data['goods_id'];
            $insertData['specs_price'] = $v;
            $insertData['specs_id'] = $k;
            $insertData['specs_num'] = $data['specs_num'][$k];
            $insertData['specs_name'] = $data['specs_name'][$k];

            GoodsSpecsList::create($insertData);

            # code...
        }
    }
}