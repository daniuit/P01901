<?php
namespace app\admin\controller;

use think\Controller;

class Index extends Common
{
    public function index()
    {
    	return $this->fetch();
    }

    public function welcome()
    {
    	return $this->fetch();
    }


    public function test()
    {
    	$arr = [
    		[4,5,6],
    		[6,7],
    		[6,7],
    		[1,2]
    	];

    	var_dump($arr);

    	$res = $this->get_data($arr);

    	var_dump($res);

    }


    public function get_data($arr)
    {
    	if(count($arr)==1){
    		return $arr[0];
    	}

    	$res = [];

    	for ($i=0; $i <count($arr) ; $i++) { 
    		
    		if($i==0){
    			$res = $arr[$i];

    			continue;
    		}

    		$temp = [];

    		foreach ($res as $k => $v) {
    			foreach ($arr[$i] as $kk => $vv) {
    				$temp[] = $v."|".$vv;
    			}
    		}

    		$res = $temp;
    	}

    	return $res;
    }

}
