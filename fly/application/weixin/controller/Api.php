<?php
namespace app\weixin\controller;

use think\Controller;

class Api extends Controller
{

	public function get_token()
	{

		if(cache('token')){
			return cache('token');
		}else{
			$appId = 'wxbac82045a637c02e';

			$appKey ='7c320fea54e77a01fcc59ff7060b04e2';
			
			$url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appId."&secret=".$appKey;

			$data = file_get_contents($url);

			$data = json_decode($data,true);

			cache('token',$data['access_token'],7100);

			return cache('token');
		}
	}

	public function test()
	{
		var_dump($this->get_token());
	}

	public function get_menu()
	{
		$url = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=".$this->get_token();

		$data = file_get_contents($url);

		echo $data;


		// $data = json_decode($data,true);

		// echo $data;exit;
	}

	public function set_menu()
	{
		$url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$this->get_token();

		$menu = [
			'button'=>[
				[
				 	"type"=>"view",
	               	"name"=>"京东",
	               	"url"=>"http://www.jd.com/"
				],
				[
					"type"=>"view",
	               	"name"=>"我的社区",
	               	"url"=>"http://1o533n8683.imwork.net/"
				],
				[
					"name"=>'个人中心',
					"sub_button"=>[
						[
							"type"=>"view",
			               	"name"=>"修改密码",
			               	"url"=>"http://www.baidu.com/"
						],
						[
							"type"=>"view",
			               	"name"=>"积分兑换",
			               	"url"=>"http://www.baidu.com/"
						],
						[
							"type"=>"view",
			               	"name"=>"我的订单",
			               	"url"=>"http://www.baidu.com/"
						],

					]
				],
			]
		];


		$this->post($url,$menu);

	}

	public function post($url,$data)
	{
		//初始化
	    $curl = curl_init();
	    //设置抓取的url
	    curl_setopt($curl, CURLOPT_URL, $url);
	    //设置头文件的信息作为数据流输出
	    curl_setopt($curl, CURLOPT_HEADER, 0);
	    //设置获取的信息以文件流的形式返回，而不是直接输出。
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    //设置post方式提交
	    curl_setopt($curl, CURLOPT_POST, 1);

	    curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($data,JSON_UNESCAPED_UNICODE));

	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); 
	    // https请求 不验证证书和hosts
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);


	    //执行命令
	    $data = curl_exec($curl);
	    //关闭URL请求
	    curl_close($curl);
		
		return json_decode($data,true);
	}


	public function get_user()
	{
		$url ="https://api.weixin.qq.com/cgi-bin/user/get?access_token=".$this->get_token();

		$data = file_get_contents($url);

		$data = json_decode($data,true);

		return $data['data']['openid'];
	}


	public function send()
	{

		$user_list = $this->get_user();
		
		$url = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=".$this->get_token();

		$data = [
			'touser'=>$user_list,
			'msgtype'=>'text',
			'text'=>[
				'content'=>'/::)'
			]
		];

		// $data = [
		// 	'touser'=>$user_list,
		// 	'msgtype'=>'voice',
		// 	'voice'=>[
		// 		'media_id'=>'qfff0G3JnT-XIFEIBZvgKCPzSTTXQHm2hYYsEx2_Woui-P7AEhv9kP5kB6vKXHCr'
		// 	]
		// ];



		// qfff0G3JnT-XIFEIBZvgKCPzSTTXQHm2hYYsEx2_Woui-P7AEhv9kP5kB6vKXHCr


		$this->post($url,$data);

	}


	public function moban()
	{
		
		$url="https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$this->get_token();


		$data = [
			'touser'=>'oF5Z2v5yzYfsCYP2oEDfz4ns94p0',
			'template_id'=>"yexZzvsr_Q4QA4GRkQXbeV2r4SV05s1bO8qU3PDTip4",
			'url'=>'http://www.baidu.com',
			'data'=>[
				'money'=>[
					'value'=>56.78,
					'color'=>'#F57900'
				],
				'time'=>[
					'value'=>date('Y-m-d H:i:s'),
					'color'=>'#F57900'
				],
				'type'=>[
					'value'=>"支付宝支付",
					'color'=>'#F57900'
				],
			]
		];

		$this->post($url,$data);
	}

	public function qr()
	{
		$url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=".$this->get_token();

		$data= [
			'expire_seconds'=>604800,
			'action_name'=>'QR_SCENE',
			'action_info'=>[
				'scene'=>[
					'scene_id'=>88888
				]
			]
		];

		$data = $this->post($url,$data);

		$url ="https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=".$data['ticket'];


		$data = file_get_contents($url);

		header("Content-type: image/jpeg"); 

		echo $data;

		exit;

		// file_put_contents('qr.jpg',$data);


	}

}
