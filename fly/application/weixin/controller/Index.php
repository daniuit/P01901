<?php
namespace app\weixin\controller;


use robots\Robot;
use think\Controller;

class Index extends Controller
{

	public function index()
	{

		if($this->checkSign()){

			if(input('echostr')){

				exit(input('echostr'));
			}else{
				
				$this->respons();
			}
		}
	}

	public function respons()
	{
		$postStr = $GLOBALS['HTTP_RAW_POST_DATA'];

		$this->log($postStr);exit;

		$postObj = simplexml_load_string($postStr,'SimpleXMLElement', LIBXML_NOCDATA);

		switch ($postObj->MsgType) {
			case 'text':
				$this->resText($postObj);
				break;
			case 'image':
				$this->resImage($postObj);
				break;
			case 'voice':
				$this->resVoice($postObj);
				break;
			case 'video':
				$this->resVideo($postObj);
				break;
			case 'location':
				$this->resLocatoin($postObj);
				break;
			default:
				$this->resText($postObj);
				break;
		}
	}

	public function resImage($postObj)
	{
		$this->log($postObj->Content);

		$data = Robot::send($postObj->Content);

		$str ="<xml>
			  <ToUserName><![CDATA[".$postObj->FromUserName."]]></ToUserName>
			  <FromUserName><![CDATA[".$postObj->ToUserName."]]></FromUserName>
			  <CreateTime>".time()."</CreateTime>
			  <MsgType><![CDATA[text]]></MsgType>
			  <Content><![CDATA[收到了]]></Content>
			</xml>";

		exit($str);
	}

	public function resText($postObj)
	{
		$this->log($postObj->Content);


		if(strstr($postObj->Content,'mp3-')){
			$this->return_mp3($postObj);
		}

		$data = Robot::send($postObj->Content);

		$str ="<xml>
			  <ToUserName><![CDATA[".$postObj->FromUserName."]]></ToUserName>
			  <FromUserName><![CDATA[".$postObj->ToUserName."]]></FromUserName>
			  <CreateTime>".time()."</CreateTime>
			  <MsgType><![CDATA[text]]></MsgType>
			  <Content><![CDATA[".$data."]]></Content>
			</xml>";

		exit($str);
	}

	public function return_mp3($postObj)
	{

		$arr = explode("-",$postObj->Content);

		$mp3 = "http://ting666.yymp3.com:86/new17/qiuyongzhuang/1.mp3";


		
		// $str ="<xml>
		// 	  <ToUserName><![CDATA[".$postObj->FromUserName."]]></ToUserName>
		// 	  <FromUserName><![CDATA[".$postObj->ToUserName."]]></FromUserName>
		// 	  <CreateTime>".time()."</CreateTime>
		// 	  <MsgType><![CDATA[text]]></MsgType>
		// 	  <Content><![CDATA[".$arr[1]."]]></Content>
		// 	</xml>";

		 $str = "<xml>
              <ToUserName><![CDATA[".$postObj->FromUserName."]]></ToUserName>
              <FromUserName><![CDATA[".$postObj->ToUserName."]]></FromUserName>
              <CreateTime>".time()."</CreateTime>
              <MsgType><![CDATA[music]]></MsgType>
               <Music>
               <Title><![CDATA[".$arr[1]."]]></Title>
               <Description><![CDATA[很好听的歌]]></Description>
               <MusicUrl><![CDATA[".$mp3."]]></MusicUrl>
               <HQMusicUrl><![CDATA[".$mp3."]]></HQMusicUrl>
               </Music>
              <FuncFlag>0</FuncFlag>
              </xml>";

		exit($str);
	}

	public function test($value='')
	{
		$res = Robot::send('火车票');

		var_dump($res);
	}

	public function log($data=array())
	{
		if(is_array($data)){
			$data = json_encode($data);
		}

		file_put_contents('./weixin.txt',$data);
	}

	public function checkSign()
	{
		return true;
		$signature = input('signature');
        $timestamp = input('timestamp');
        $nonce = input('nonce');
        $token = "xbs";

        $tmpArr = array($token, $timestamp, $nonce);

        sort($tmpArr);

        $tmpStr = implode($tmpArr);

        $tmpStr = sha1($tmpStr);
        
        if( $tmpStr == $signature ){
            return true;
        }else{
            return false;
        }
	}

}