<?php
namespace app\api\controller;

use app\index\model\User as UserModel;
use think\Controller;
/**
 * @Author: xuebingsi
 * @Date:   2019-04-23 16:01:48
 * @Last Modified by:   zhibinm
 * @Last Modified time: 2019-04-23 16:06:36
 */
/**
 * 
 */
class User extends Controller
{
	
	public function get()
	{
		if(input('uid')){

			$user = UserModel::get(input('uid'));

			if($user){
				exit(json_encode(['code'=>0,'result'=>$user]));
			}else{
				exit(json_encode(['code'=>40001,'msg'=>'无该用户']));
			}

		}else{
			exit(json_encode(['code'=>40000,'msg'=>'参数缺件']));
		}
	}
}