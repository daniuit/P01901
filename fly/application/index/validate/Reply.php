<?php
namespace app\index\validate;

use app\index\model\User;
use think\Validate;

class Reply extends Validate
{
    protected $rule = [
        'content|内容'=>'require|min:10',
        'question_id|问题id'=>'require|integer'
    ];
}
