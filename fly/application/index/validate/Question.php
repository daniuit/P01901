<?php
namespace app\index\validate;

use app\index\model\User;
use think\Validate;

class Question extends Validate
{
    protected $rule = [
        // 'captcha'=>'require|captcha',
        'title|标题'=>'require|min:10',
        'content|内容'=>'require|min:10',
        'cate_id|分类'=>'require|integer',
        'kiss|飞吻'=>'require|integer|checkkiss',
    ];

    public function checkkiss($value)
    {
    	$user = User::get(session('user_id'));
    	
    	if($user->kiss>=$value){
    		return true;
    	}else{
    		return "飞吻不够，请先充值!";
    	}
    }
}
