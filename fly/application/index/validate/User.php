<?php
namespace app\index\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'captcha'=>'require|captcha',
        'phone|手机'=>'require|unique:user,phone',
        'nickname|昵称'=>'require|length:6,8|unique:user,nickname',
        'agreement|协议'=>'require|accepted',
        'email|邮箱'=>'email',
        'password|密码'=>'require',
        'repassword|确认密码'=>'require|confirm:password',
        'vercode|手机验证码'=>'require|checkCode',
    ];

    protected $message =[
    	'agreement.require'=>'服务条款必须同意'
    ];

    protected $scene = [
        'save'  =>  ['phone','nickname','email'],
        'password'  =>  ['password','repassword'],
        'send'  =>  ['phone','captcha'],
        'reg'  =>  ['phone','nickname','agreement','email','password','repassword','vercode'],
    ];

    public function checkCode($value,$rule,$data)
    {

        if(!session('code')){
            return "先获取验证码";
        }
        
        if(time()-session('exp')>300){
            return "验证码已过期";
        }

        if($value==session('code')&&session('phone')==$data['phone']){
            return true;
        }else{
            return '验证码错误';
        }
        
    }
}
