<?php
namespace app\index\model;

use think\Model;

class Sign extends Model
{
	protected $autoWriteTimestamp = true;

	public function get_sign_info()
	{
		$data = [];

		if(session('user_id')){

			$data['is_login']=true;
			$data['kiss'] = 5;
			$data['num'] = 0;
			$data['is_sign'] = false;

			$sign = $this->where('user_id',session('user_id'))->find();

			if($sign && $sign->sign_date==date('Y-m-d')){
				$data['is_sign'] = true;
				$data['num'] = $sign->sign_num;
			}elseif($sign){
				$data['is_sign'] = false;

				if($sign->sign_date==date('Y-m-d',strtotime('-1 days'))){
					
					$data['kiss'] = $this->get_kiss($sign->sign_num+1);
					$data['num'] = $sign->sign_num;
				}
			}

			return $data;

		}else{
			return ['is_login'=>false];
		}
	}

	public function get_kiss($num)
	{
		if($num<=5){
			return 5;
		}elseif($num<=10){
			return 10;
		}elseif($num<=15){
			return 15;
		}else{
			return 20;
		}
	}
}