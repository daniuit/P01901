<?php
namespace app\index\model;


use think\Db;
use think\Model;

class Cate extends Model
{
	public static function get_cate_tree()
	{
		if(cache('cates')){
			return cache('cates');
		}else{

			$data = Db::table('bbs_shop_cate')->order('order_by desc')->select();

			$temp = [];

			foreach ($data as $k => $v) {

				if($v['fid']==0){

					$v['son1'] = [];

					foreach ($data as $kk => $vv) {
					 	if($vv['fid']==$v['cate_id']){

					 		$vv['son2'] = [];
					 		foreach ($data as $kkk => $vvv) {
					 			if($vvv['fid']==$vv['cate_id']){
					 				$vv['son2'][] = $vvv;
					 			}
					 		}
					 		$v['son1'][] = $vv;
					 	}
					 } 

					$temp[] = $v;
				}
			}

			cache('cates',$temp,86400);

			return $temp;
		}

	}
}