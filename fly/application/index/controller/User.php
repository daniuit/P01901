<?php
namespace app\index\controller;

use app\index\model\Sign;
use app\index\model\User as UserModel;
use think\Controller;
use think\Db;
use think\Loader;

class User extends Common
{

    public function show()
    {
        var_dump(input('user_id'));
    }
    public function center()
    {

        $sign_info = (new Sign)->get_sign_info();

        $this->assign('sign',$sign_info);
       	return $this->fetch();
    }

    public function sign()
    {
        
        $sign_info = (new Sign)->get_sign_info();

        if(!$sign_info['is_login']){
            $this->error('请先登录');
        }

        if($sign_info['is_sign']){
            $this->error('今天已签到过');
        }

        $sign = Sign::get(['user_id'=>session('user_id')]);

        if($sign){
            $sign->sign_num = $sign_info['num']+1;
            $sign->ctime = time();
            $sign->sign_date = date('Y-m-d');
            $sign->save();
        }else{
            $data['user_id'] = session('user_id');
            $data['ctime'] = time();
            $data['sign_date'] = date('Y-m-d');
            $data['sign_num'] = 1;
            Sign::create($data);
        }

        Db::name('user')->where('user_id',session('user_id'))->setInc('kiss',$sign_info['kiss']);

        $this->success("签到成功");

    }

    public function set()
    {
    	$user = UserModel::get(session('user_id'));

    	$this->assign('user',$user);
    	$this->assign('a',90);

    	return $this->fetch();
    }

    public function upload()
    {
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('file');
        
        // 移动到框架应用根目录/public/uploads/ 目录下
        if($file){
            $info = $file->validate(['size'=>20000,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
            if($info){
                // 成功上传后 获取上传信息
                $face = "uploads/".date("Ymd")."/".$info->getFilename();

                $res = UserModel::where('user_id',session('user_id'))->update(['face'=>$face]);

                if(session('face')!="static/index/images/face.jpg"){
                    unlink(session('face'));
                }
                
                session('face',$face);

                $this->error('上传成功',null,['src'=>$face]);

                // exit(json_encode(['code'=>0,'msg'=>'上传成功','data'=>['src'=>$face]]));
            }else{
                // 上传失败获取错误信息
                $this->success($file->getError());
                // exit(json_encode(['code'=>1,'msg'=>$file->getError()]));
            }
        }
       
    }

    public function save()
    {
    	$data = input('post.');

    	$validate = Loader::validate('User');

    	$data['user_id'] = session('user_id');

        if(!$validate->scene('save')->check($data)){
            $this->error($validate->getError());
        }

        $res = UserModel::where('user_id',session('user_id'))->update($data);

        if($res){
        	$this->success('更新成功');
        }else{
        	$this->error('数据未变更');
        }
    }

    public function tie()
    {
    	return $this->fetch();
    }

    public function password()
    {
        $data = input('post.');

        $validate = Loader::validate('User');

        $data['user_id'] = session('user_id');

        if(!$validate->scene('password')->check($data)){
            $this->error($validate->getError());
        }

        $data['password'] = md5($data['password']);

        $res = UserModel::where('user_id',session('user_id'))->update($data);

        if($res){
            $this->success('更新成功');
        }else{
            $this->error('数据未变更');
        }
    }

    
}