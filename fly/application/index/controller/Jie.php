<?php
namespace app\index\controller;

use app\index\model\Cate;
use app\index\model\Collect;
use app\index\model\Question;
use app\index\model\Reply;
use app\index\model\Zan;
use think\Controller;
use think\Db;
use think\Loader;

class Jie extends Common
{

    
    public function add()
    {
    	$cates = Cate::all();

    	$this->assign('cates',$cates);
       	return $this->fetch();
    }

    public function test()
    {
        // sleep(6);
        
        Db::transaction(function(){
            Db::table('stu')->insert(['name'=>'xbs999']);
            Db::table('stu')->delete(1);
        });
    }
    

    public function show()
    {


        $uid = session('user_id')?session('user_id'):'null';

        $qid = intval(input('qid'));

    //     Db::query('select * from think_user where id=?',[8]);


    //     $sql = "select * from bbs_question where qid='{$qid}'";

    //     var_dump($sql);exit;

    //     $data = Db::name('question')->where('question_id',$qid)->find();

    //     // $data = Question::get($qid);
    //     // 
    // var_dump($data);

    //     var_dump($sql);

    //     var_dump($qid);


    //     exit;

        Db::name('question')->where('question_id', $qid)->setInc('view_num');

        $data = Db::name('question')
        ->alias('t1')
        ->field('t1.*,t2.nickname,t2.face,t3.cate_name,t4.collect_id')
        ->join('user t2','t1.user_id = t2.user_id')
        ->join('cate t3','t3.cate_id = t1.cate_id')
        ->join('collect t4',"t1.question_id=t4.question_id and t4.user_id={$uid}",'left')
        ->where('t1.question_id',$qid)
        ->find();

         $replys = Db::name('reply')
        ->alias('t1')
        ->field('t1.*,t2.nickname,t2.face,t3.zan_id')
        ->join('user t2','t1.user_id = t2.user_id')
        ->join('zan t3',"t1.reply_id=t3.reply_id and t3.user_id={$uid}",'left')
        ->where('t1.question_id',$qid)
        ->order('status desc,reply_id desc')
        ->paginate(2);

        


        // foreach ($replys as $k => $v) {
        //    $zan = Zan::get(['reply_id'=>$v['reply_id'],'user_id'=>session('user_id')]);
        //    if($zan){
        //      $replys[$k]['is_zan'] = true;
        //    }else{
        //       $replys[$k]['is_zan'] = false;  
        //    }
        // }

        // var_dump($replys);

        $week_hot = Question::where(['create_time'=>['>=',strtotime('-7 days')]])->order('reply_num desc')->limit(10)->select();

        $this->assign('week_hot',$week_hot);
        $this->assign('data',$data);
        $this->assign('replys',$replys);
        $this->assign('title',$data['title']);

        return $this->fetch();
    }

    public function cai()
    {
        $reply_id = input('post.reply_id');

        $reply = Reply::get($reply_id);

        if($reply && $reply->user_id==session('user_id')){
            $this->error('自己不能采纳自己的');
        }

        $reply->status = '1';

        $reply->save();

        $question = Question::get($reply->question_id);

        $question->status = '1';

        $question->save();

        Db::name('user')->where('user_id',$reply->user_id)->setInc('kiss', $question->kiss);

        $this->success('采纳成功');

    }

    public function collect()
    {
        $question_id = input('post.question_id');

        $collect = Collect::get(['question_id'=>$question_id,'user_id'=>session('user_id')]);

        if($collect){
            
            $collect->delete();

            $this->success('取消收藏');

        }else{

            Collect::create(['question_id'=>$question_id,'user_id'=>session('user_id')]);

            $this->success('收藏成功');
        }

    }

    public function zan()
    {

        $reply_id = input('post.reply_id');


        $zan = Zan::get(['reply_id'=>$reply_id,'user_id'=>session('user_id')]);

        if($zan){
            $this->error('你已经点赞过');
        }else{
            Db::name('reply')->where('reply_id', $reply_id)->setInc('ok_num');

            Zan::create(['reply_id'=>$reply_id,'user_id'=>session('user_id')]);

            $this->success('点赞成功');
        }
    }

    public function save()
    {
    	$data = input('post.');

        $validate = Loader::validate('Question');

        if(!$validate->check($data)){
            $this->error($validate->getError());
        }

        if($data['cate_id']=='1'){
        	$data['ext_info'] = json_encode($data['ext_info']);
        }else{
        	unset($data['ext_info']);
        }

        $data['user_id'] = session('user_id');

        $res = Question::create($data);
 
        if($res){
            Db::name('user')->where('user_id', session('user_id'))->setDec('kiss', $data['kiss']);
           $this->success("发布成功",url('index/jie/show',['qid'=>$res->question_id]));  
        }else{
            $this->error('发布失败');
        }
    }

    public function upload()
    {
    	#// 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('file');
        
        // 移动到框架应用根目录/public/uploads/ 目录下
        if($file){
            $info = $file->validate(['size'=>2000000,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads/jie');
            if($info){
                // 成功上传后 获取上传信息
                $face = "/uploads/jie/".date("Ymd")."/".$info->getFilename();

                $this->error('上传成功',null,['src'=>$face]);
            }else{
                // 上传失败获取错误信息
                $this->success($file->getError());
            }
        }
    }

    public function reply()
    {
        $data = input('post.');

        $validate = Loader::validate('Reply');

        if(!$validate->check($data)){
            $this->error($validate->getError());
        }

        $data['user_id'] = session('user_id');

        $res = Reply::create($data);
 
        if($res){
            Db::name('question')->where('question_id', $data['question_id'])->setInc('reply_num');
            $this->success("发布成功");  
        }else{
            $this->error('发布失败');
        }
    }
}