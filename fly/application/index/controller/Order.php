<?php
namespace app\index\controller;

use app\index\model\Cate;
use think\Db;

require_once '../vendor/alipay/pagepay/service/AlipayTradeService.php';
require_once '../vendor/alipay/pagepay/buildermodel/AlipayTradePagePayContentBuilder.php';

class Order extends Common
{
	public function index()
	{
		$config = config('alipay');

		$no = "20190450145454";
		$title = "xxx 某某商品";
		$price = '0.01';
		$info = " xxxx 老司机机社区订单";

		//构造参数
		$payRequestBuilder = new \AlipayTradePagePayContentBuilder();
		$payRequestBuilder->setBody($info);
		$payRequestBuilder->setSubject($title);
		$payRequestBuilder->setTotalAmount($price);
		$payRequestBuilder->setOutTradeNo($no);

		$aop = new \AlipayTradeService($config);

		/**
		 * pagePay 电脑网站支付请求
		 * @param $builder 业务参数，使用buildmodel中的对象生成。
		 * @param $return_url 同步跳转地址，公网可以访问
		 * @param $notify_url 异步通知地址，公网可以访问
		 * @return $response 支付宝返回的信息
	 	*/
		$response = $aop->pagePay($payRequestBuilder,$config['return_url'],$config['notify_url']);

		//输出表单
		var_dump($response);exit;

	}

	public function return_url()
	{

		$config = config('alipay');

		$arr=$_GET;

		$alipaySevice = new \AlipayTradeService($config); 

		$result = $alipaySevice->check($arr);

		/* 实际验证过程建议商户添加以下校验。
		1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
		2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
		3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
		4、验证app_id是否为该商户本身。
		*/
		if($result) {//验证成功

			echo "验证成功<br />支付宝交易号：";
			var_dump($_GET);
		}
	}

	public function notice_url()
	{

		$config = config('alipay');

		$arr=$_POST;

		$alipaySevice = new \AlipayTradeService($config); 

		$result = $alipaySevice->check($arr);

		/* 实际验证过程建议商户添加以下校验。
		1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
		2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
		3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
		4、验证app_id是否为该商户本身。
		*/
		if($result) {//验证成功

			file_put_contents('./test.txt',json_encode($arr));

		}
	}
}