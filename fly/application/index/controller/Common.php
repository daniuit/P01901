<?php
namespace app\index\controller;

use think\Controller;

class Common extends Controller
{

	public function _initialize()
    {

        if(strstr($_SERVER['HTTP_USER_AGENT'],'MicroMessenger') && !session('user_id')){

            $this->redirect('index/login/weixin');
            
        }else{

            $rule = strtolower(request()->module()."/".request()->controller()."/".request()->action());

            $auth = [
            	'index/jie/add',
            	'index/jie/zan',
            	'index/jie/reply',
            	'index/jie/collet',
            	'index/user/set',
            	'index/user/center',
            ];

            if(in_array($rule,$auth)){
            	if(!session('user_id')){
            		$this->redirect('index/login/weixin');
            	}
            }

        }
    }
}