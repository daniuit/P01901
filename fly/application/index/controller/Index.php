<?php
namespace app\index\controller;

use alimsg\Sms;
use app\index\model\Sign;
use app\index\model\Cate;
use app\index\model\Question;
use robots\Robot;
use think\Controller;
use think\Db;

class Index extends Common
{

    public function test()
    {

        $res = Robot::send(input('p'));

        var_dump($res);

        Sms::send('18925139194','67809');
        // $ip = "219.136.2.117";

        // $url = "http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;

        // $data = file_get_contents($url);

        // var_dump(json_decode($data,true));
    }

    public function tianqi()
    {
        $host = "http://weatherq.market.alicloudapi.com";
        $path = "/clouds/query/weather/details";
        $method = "GET";
        $appcode = "ab7bfa4f7d544241998ea5d8a8ddb028";
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = "cityNameOrId=广州";
        $bodys = "";
        $url = $host . $path . "?" . $querys;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        $data = curl_exec($curl);

        var_dump($data);

        var_dump(json_decode($data,true));
    }

    public function kuaidi()
    {
        $host = "https://courier.market.alicloudapi.com";
        $path = "/courier";
        $method = "GET";
        $appcode = "ab7bfa4f7d544241998ea5d8a8ddb028";
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = "com=auto&no=3704748859365";
        $bodys = "";
        $url = $host . $path . "?" . $querys;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        $data = curl_exec($curl);
        var_dump($data);

        var_dump(json_decode($data,true)['result']['data']);
    }
    public function index()
    {

        cache('title','888',60);
        // 
        // var_dump(cache('title'));exit;


    	if(!input('')){
    		$top = Db::name('question')
	        ->alias('t1')
	        ->field('t1.*,t2.nickname,t2.face,t3.cate_name')
	        ->join('user t2','t1.user_id = t2.user_id')
	        ->join('cate t3','t3.cate_id = t1.cate_id')
	        ->where('t1.is_top','1')
            ->cache(3600)
	        ->limit(4)
	        ->select();

	        $this->assign('top',$top);
    	}
    	

    	$where = [];

    	if(input('cid')){
    		$where['t1.cate_id'] = input('cid');
    	}

    	if(input('type')=='2'){
    		$where['t1.status'] = '1';
    	}

    	if(input('type')=='1'){
    		$where['t1.status'] = '0';
    	}

    	if(input('type')=='3'){
    		$where['t1.is_good'] = '1';
    	}

        $data = Db::name('question')
        ->alias('t1')
        ->field('t1.*,t2.nickname,t2.face,t2.level,t3.cate_name')
        ->join('user t2','t1.user_id = t2.user_id')
        ->join('cate t3','t3.cate_id = t1.cate_id')
        ->where($where)
        ->order('t1.question_id desc')
        ->paginate(10);


        $week_hot = Question::where(['create_time'=>['>=',strtotime(date('Y-m-d',strtotime('-7 days')))]])->order('reply_num desc')->limit(10)->cache(3600)->select();


        // var_dump($week_hot);


        $week_reply = Db::name('reply')
        ->alias('t1')
        ->field('count(t1.reply_id) cn,t1.user_id,t2.nickname,t2.face')
        ->join('user t2','t1.user_id = t2.user_id')
        ->where(['t1.create_time'=>['>=',strtotime(date('Y-m-d',strtotime('-7 days')))]])
        ->group('t1.user_id')
        ->cache(3600)
        ->order('cn desc')
        ->limit(12)
        ->select();


    	$cates = Cate::order('orderby desc')->cache(3600)->select();


        $sign_info = (new Sign)->get_sign_info();


        $this->assign('sign',$sign_info);
    	$this->assign('week_reply',$week_reply);
    	$this->assign('cates',$cates);
    	$this->assign('week_hot',$week_hot);
        $this->assign('data',$data);
    	$this->assign('title',"一个专业的讨论司机的论坛");
    	

       	return $this->fetch();
    }

    public function show()
    {

    	echo "index-show";
    }
    

}
