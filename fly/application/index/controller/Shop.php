<?php
namespace app\index\controller;

use app\index\model\Cate;
use think\Db;

class Shop extends Common
{
	public function index()
	{
		

		$nav_data = Db::table('bbs_shop_cate')->cache(3600)->where('is_nav','1')->select();


		// $temp = $this->get_cate_tree($data);

		// var_dump($temp);exit;


		// var_dump($data);

		$temp =  Cate::get_cate_tree();

		$this->assign('cates',$temp);
		$this->assign('nav_data',$nav_data);

		return $this->fetch();
	}

	public function get_cate_tree($data,$fid='0')
	{
		$temp = [];

		foreach ($data as $k => $v) {
			if($v['fid']==$fid){
				$v['son'] = $this->get_cate_tree($data,$v['cate_id']);
				$temp[] = $v;
			}
		}
		return $temp;
	}
}