<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
function time_format($time)
{
	$diff = time()-$time;

	if($diff<60){
		return "刚刚";
	}elseif($diff<3600){
		return floor($diff/60)."分钟前";
	}elseif($diff<86400){
		return floor($diff/3600)."小时前";
	}elseif($diff<(86400*7)){
		return floor($diff/86400)."天前";
	}else{
		return date("Y-m-d H:i:s",$time);
	}
	
}

function di_ka_er_ji($arr)
    {
    	if(count($arr)==1){
    		return $arr[0];
    	}

    	$res = [];

    	for ($i=0; $i <count($arr) ; $i++) { 
    		
    		if($i==0){
    			$res = $arr[$i];

    			continue;
    		}

    		$temp = [];

    		foreach ($res as $k => $v) {
    			foreach ($arr[$i] as $kk => $vv) {
    				$temp[] = $v."|".$vv;
    			}
    		}

    		$res = $temp;
    	}

    	return $res;
    }