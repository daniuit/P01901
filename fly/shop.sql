/*
* @Author: xuebingsi
* @Date:   2019-05-15 10:03:49
* @Last Modified by:   zhibinm
* @Last Modified time: 2019-05-15 10:45:42
*/
CREATE TABLE bbs_goods (
  `goods_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` char(30) NOT NULL COMMENT '标题',
  `intro` char(30) NOT NULL COMMENT '副标题',
  `content` text NOT NULL COMMENT '内容',
  	shop_price DECIMAL(7, 3) COMMENT "商品价格",
  	market_price DECIMAL(7, 3) COMMENT "市场价格",
  	score int(7) DEFAULT 0 COMMENT "积分",
  	shop_image char(255) COMMENT '主图',
  	shop_image_list varchar(1000) COMMENT '缩略图',
  `cate_id` int(10) unsigned NOT NULL COMMENT '分类',
  `brand_id` int(10) unsigned NOT NULL COMMENT '品牌id',
  `status` enum('0','1') DEFAULT '0' COMMENT '状态 0 下载 1 上架',
  `is_hot` enum('0','1') DEFAULT '0' COMMENT '0 正常 1 热销',
  `is_new` enum('0','1') DEFAULT '0' COMMENT '0 正常 1 新品',
  `create_time` int(10) unsigned DEFAULT NULL COMMENT '创建时间',=
  `shop_attr` text DEFAULT NULL COMMENT '商品属性',
  PRIMARY KEY (`goods_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;