<?php

namespace robots;
/**
 * 
 */
class Robot 
{
	
	public static function send($text)
	{
		$url ="http://openapi.tuling123.com/openapi/api/v2";

		$method = "POST";

		$data= [
			'reqType'=>'0',
			'perception'=>[
				'inputText'=>[
					'text'=>$text
				]
			],
			'userInfo'=>[
				'apiKey'=>'facc47b9f3afc7a493121bf6aacab53f',
				'userId'=>'1'
			]

		];

		$curl = curl_init();
	    //设置抓取的url
	    curl_setopt($curl, CURLOPT_URL, $url);
	    //设置头文件的信息作为数据流输出
	    curl_setopt($curl, CURLOPT_HEADER, 0);
	    //设置获取的信息以文件流的形式返回，而不是直接输出。
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    //设置post方式提交
	    curl_setopt($curl, CURLOPT_POST, 1);
	    //设置post数据
	    curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($data));

	    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json',
		    'Content-Length: ' . strlen(json_encode($data))
		));

	    //执行命令
	    $data = curl_exec($curl);
	    //关闭URL请求
	    curl_close($curl);
	    //显示获得的数据
	    $data = current(json_decode($data,true)['results']);

	    var_dump($data);

	    return isset($data['values']['text'])?$data['values']['text']:"<a href='".$data['values']['url']."'>".$text."</a>";

	}
}