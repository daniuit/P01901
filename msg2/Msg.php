<?php
/**
 * 
 */
class Msg 
{

	public $path = "./db/data.txt";
	
	public function add($value='')
	{

		if(!isset($_SESSION['username'])){
			header('Location:./index.php');
			exit;
		}
		
		$data = $_POST;

		$data['username'] = $_SESSION['username'];
		$data['ctime'] = time();

		$oldData =  $this->get_data();

		$oldData[] = $data;

		$this->put_data($oldData);
	}

	protected function put_data($data)
	{
		file_put_contents($this->path,json_encode($data));

		header("Location:./index.php");
	}

	public function edit()
	{
		$id = $_GET['id'];

		$oldData = $this->get_data();

		$data = $oldData[$id];

		include './template/edit.html';
	}

	public function del()
	{
		$id = $_GET['id'];

		$oldData = $this->get_data();

		unset($oldData[$id]);

		$this->put_data($oldData);

	}

	public function save()
	{
		$id = $_GET['id'];

		$data = $_POST;

		$oldData = $this->get_data();

		$oldData[$id] = $data;

		$this->put_data($oldData);
	}

	public function show()
	{
		
		$data = $this->get_data();

		krsort($data);

		$data = array_slice($data,0,3,true);

		include './template/index.html';
		var_dump($_SESSION);
	}

	public function more()
	{
		$page = $_POST['page'];

		$data = $this->get_data();

		krsort($data);

		$data = array_slice($data,($page-1)*3,3,true);


		if(!$data){
			echo "<p >我是有底线的！！！</p>";
		}

		include './template/more.html';
	}

	protected function get_data()
	{
		$data = file_get_contents($this->path);

		return $data = json_decode($data,true);
	}
}