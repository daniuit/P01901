打一个系统骨架（框架)

单入口

整个系统都是从这个文件进去，通过路由确定要访问的功能

index.php?m=index&c=user&a=edit
index.php?m=index&c=user&a=save
index.php?m=index&c=user&a=login

index.php?m=admin&c=user&a=del
index.php?m=admin&c=user&a=stop
index.php?m=admin&c=user&a=edit

mvc

模型(model)－视图(view)－控制器(controller)

模型 代表你的数据结构。通常来说，模型类将包含帮助你对数据库进行增删改查的方法。

视图 是要展现给用户的信息。一个视图通常就是一个网页

控制器 是模型、视图以及其他任何处理 HTTP 请求所必须的资源之间的中介，并生成网页。

一个相关功能方法的集合（类）

模块

对应用功能进行划分

路由

决定访问什么功能

应用（项目 系统 程序）\

分配变量

加载视图







