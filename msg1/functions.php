<?php

function time_format($time)
{
	$diff = time()-$time;

	if($diff<60){
		return "刚刚";
	}elseif($diff<3600){
		return floor($diff/60)."分钟前";
	}elseif($diff<86400){
		return floor($diff/3600)."小时前";
	}elseif($diff<(86400*7)){
		return floor($diff/86400)."天前";
	}else{
		return date("Y-m-d H:i:s",$time);
	}
	
}