/*
* @Author: xuebingsi
* @Date:   2019-04-01 10:55:22
* @Last Modified by:   zhibinm
* @Last Modified time: 2019-04-01 15:13:09
*/
-- 查询数据库
show databases;
-- 创建数据库
create database school default character set utf8;
-- 删除数据库
drop database school;
-- 使用数据库
use school;

-- 创建表
create table user(
	id int(7) unsigned primary key AUTO_INCREMENT,
	name char(30),
	sex enum('男','女','人妖'),
	phone char(11) unique,
	addr char(40),
	email char(50),
	passwrod char(32),
	ctime int(10) unsigned,
	age TINYINT unsigned,
	poundage DECIMAL(3,3)
);
-- 查看表
show tables;
-- 结构
desc user;

--
-- 标题 内容  用户 时间 浏览量 回复量 点赞数量 帖子ID 类型 

create table tie(
	id int(10) unsigned primary key AUTO_INCREMENT COMMENT "主键",
	title char(30) unique COMMENT "标题",
	content varchar(1000) COMMENT "内容",
	user char(20) COMMENT "用户",
	ctime int(10) unsigned COMMENT "创建时间",
	view_num int(7) unsigned zerofill default 0 COMMENT "浏览量" ,
	replay_num int(5) unsigned default 0,
	ok_num int(10) unsigned default 0,
	cate char(10) not null
);


drop table user;

-- 服务端数据库的编码 uft8;

set names gbk;


select * from user;


insert into user(name,age,sex,passwrod) values('小明',18,'男','234234234');


update user set  phone='123123123123' where id=1;
update user set  phone='888',age=30 where id=2;


delete from user where id=3;

update user set sex='女' where id<4;