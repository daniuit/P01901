<?php

function a($num)
{
	if($num==1){
		return 1;
	}
	return $num+a($num-1);
}


$res = a(5);

echo $res;

// 10+9+8+7+6+5+4+3+2+1

// 10 + a(9)

// 9 +a(8)

// 8+a(7)

// function a(5)
// {
// 	if(5==1){
// 		return 1;
// 	}
// 	return 5+a(5-1);
// }

// function a(4)
// {
// 	if(4==1){
// 		return 1;
// 	}
// 	return 4+a(4-1);
// }

// function a(3)
// {
// 	if(3==1){
// 		return 1;
// 	}
// 	return 3+a(3-1);
// }

// function a(2)
// {
// 	if(2==1){
// 		return 1;
// 	}
// 	return 2+a(2-1);
// }

// function a(1)
// {
// 	if(1==1){
// 		return 1;
// 	}
// 	return 1+a(3-1);
// }