<?php

// 1 [[345,345,345,23,45],[45,3,34,345],[56,3,3]]

function get_sum($arr){
	$sum = 0;
	foreach ($arr as $k => $v) {
		
		foreach ($v as $kk => $vv) {
			$sum +=$vv;
		}
	}
	return $sum;
}


// $res = get_sum([[345,345,345,23,45],[45,3,34,345],[56,3,3]]);

// var_dump($res);
// 封装一个函数进行多维数组求和

// 2 把 1-100 中的奇数放到一个数组中

function get_ji($num=100,$c=0)
{
	$arr = [];
	for ($i=1; $i <=$num ; $i++) { 
		if($c==0){
			if($i%2==1){
				$arr[] = $i;
			}
		}else{
			if($i%$c==0){
				$arr[] = $i;
			}
		}
	}
	return $arr;
}

// $res = get_ji(200,15);

// var_dump($res);

// 3 封装一个函数，实现数组的去重 [564,3,7,346,3,5,56,5,5]
// 

function quchong($arr){
	$temp = [];

	foreach ($arr as $v) {

		if(!in_array($v,$temp)){
			$temp[]= $v;
		}
	}

	return $temp;

}
// $res = quchong([564,3,3,3,3,3,3,37,346,3,5,56,5,5]);

// var_dump($res);
// // in_array()

// 4 封装一个函数，返回最大值 与最小值 的差 [34,2,2,7,32,657,56]
// 
function max_min_diff($arr){

	$max = $arr[0];
	$min = $arr[0];

	foreach ($arr as  $v) {
		if($v>$max){
			$max = $v;
		}

		if($v<$min){
			$min =$v;
		}
	}
	return $max-$min;
}

$res = max_min_diff([34,2,2,7,32,657,56]);

// var_dump($res);

// 5 重组数组 

// 原数组  [['name'=>'小明','age'=>'18','sex'=>'男'],['name'=>'小红','age'=>'20','sex'=>'女']] 

// 重组后数组 ['name'=>['小明','小红'],'age'=>['18','20'],'sex'=>['男','女']]

$arr = [['name'=>'小明','age'=>'18','sex'=>'男'],['name'=>'小红','age'=>'20','sex'=>'女']];

function chongzu_arr($arr)
{
	$temp = [];

	foreach ($arr as  $v) {
		foreach ($v as $k => $vv) {
			$temp[$k][] = $vv;
		}
	}
	return $temp;
}

$res = chongzu_arr($arr);

var_dump($res);

// var_dump(['name'=>['小明','小红'],'age'=>['18','20'],'sex'=>['男','女']]);