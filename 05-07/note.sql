/*
* @Author: xuebingsi
* @Date:   2019-05-07 15:05:54
* @Last Modified by:   zhibinm
* @Last Modified time: 2019-05-07 17:40:36
*/
mysql 安全


1  帐号密码安全

2  权限控制 帐号控制   请求ip 控制

3  用户输入  sql注入


存储引擎 mysql 数据存储的算法不一样

MyISAM 读得快  表锁


InnoDB  写得快  行锁


事务 是为了保证一件事情完整性


InnoDB 支持


视图  虚拟表 没有任何实际存在数据


触发器

当对数据进行 增 删 改 时候可以同时去执行一些语句


CREATE TRIGGER after_insert_stu 
AFTER INSERT ON stu 
FOR EACH ROW 
insert into log(stu_id,ctime) values(new.id,now());




CREATE TRIGGER after_delete_stu 
AFTER INSERT ON stu 
FOR EACH ROW 
delete from log where stu_id = old.id;



存储过程

把一些sql语句封闭起来，可以重复调用的代码块

DELIMITER //  

CREATE PROCEDURE getstu(OUT s int)  
BEGIN 
	SELECT count(id) INTO s FROM stu; 
END

//  
DELIMITER ; 
call getcountstu(@s);
select @s;

存储函数

CREATE FUNCTION get_max(num1 INT,num2 INT) 
RETURNS INT
BEGIN
	IF num1 >=num2 THEN
		RETURN num1; 
	ELSE
		RETURN num2; 
	END IF; 
END//


mysql 优化

存储字段

1 越简单越好 
2 越小越好 

表设计

分库

分表

水平分表

order 300

order201901   100
order201902    100
order201903    100

垂直分表

io读写

减少读写

*  尽量不要使用 *
使用缓存 

索引 

对数据进行排序，可以通过算法快速定位到数据

坏处，占硬盘空间，增删改效率降低

需要经常查询的字段才建议索引 


慢查询 


select sleep(3);


