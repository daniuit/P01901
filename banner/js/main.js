// 页面加载完再执行
window.onload = function () {
	// 捉标签
	img_item = document.getElementsByClassName('img_item');
	click_btn = document.getElementsByClassName('click_btn');

	click_left = document.getElementById('click_left');
	click_right = document.getElementById('click_right');
	banner = document.getElementById('banner');

	// 控制显示第几个
	num = 0;

	// 执行显示第几个
	function run() {

		// 把图片与小点进行统一消失
		for (var i = 0; i < img_item.length; i++) {
			img_item[i].style.display= "none";
			click_btn[i].style.background= "rgba(0,0,0,0.6)";
		}

		// 让第num出现
		img_item[num].style.display= "block";
		click_btn[num].style.background= "rgba(255,255,255,0.8)";

		// 是否是最后一个，是的话重新来，不是自增
		if(num==img_item.length-1){
			num = 0;
		}else{
			num++;
		}
	}

	// 开始定时器
	time = setInterval(run,1000);

	// 进行这个区域停定时器
	banner.onmouseover= function () {
		clearInterval(time)
	}
	// 离开这个区域开始定时器
	banner.onmouseout= function () {
		time = setInterval(run,1000);
	}

	// 向右走的逻辑
	click_right.onclick= function () {
		run();
	}
	// 向左走逻辑
	click_left.onclick= function () {
		num -=2;
		run();
	}

	//循环给所有点加点击事件
	for (var i = 0; i <click_btn.length; i++) {
		// 保存每个点的对应下标
		click_btn[i].index = i;
		click_btn[i].onclick= function () {
			num =this.index
			run();
		}
	}
}