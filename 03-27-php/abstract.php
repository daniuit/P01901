<?php

abstract  class A 
{
	abstract public function run();
	abstract public function jiao();

	public function chi()
	{
		# code...
	}
}

/**
 * 
 */
class B extends A
{
	public function run()
	{
		# code...
	}

	public function jiao()
	{
		# code...
	}
}

class C extends A
{
	public function run()
	{
		# code...
	}

	public function jiao()
	{
		# code...
	}
}

$c= new C();

var_dump($c);