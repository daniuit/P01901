<?php

/**
 * Cat
 */
class Cat extends Animal
{
	public $name;
	private function chi1()
	{
		echo 'chi';
	}
	private function chi2()
	{
		echo 'chi';
	}
	private function chi3()
	{
		echo 'chi';
	}

	public function jiao()
	{
		$this->run();
		$this->chi1();
		echo "cat-jiao";
		$this->chi2();
		$this->chi3();
	}

}