<?php
// 引入全局函数库
include './functions.php';
// 读取数据库数据
$data = file_get_contents("./db/data.txt");
// 把json数据转成数组 
$data = json_decode($data,true);
// 排序
krsort($data);
// 引模板
include './template/index.html';
