/*
* @Author: xuebingsi
* @Date:   2019-04-03 10:36:08
* @Last Modified by:   zhibinm
* @Last Modified time: 2019-04-03 14:24:56
*/
英语成绩最高的

select max(s_score) from score where c_id=(select c_id  from course where c_name='英语');

查询平均成绩大于等于60分的同学的学生编号

select avg(s_score) ss,s_id from score group by s_id having ss>=60;

查询所有同学课程的平均分

select avg(s_score) ss,s_id from score group by s_id;

查询有一课不及格的同学

select DISTINCT s_id from score where s_score <60;

按平均分的从高到低进行排序 

select avg(s_score) ss,s_id from score group by s_id order by ss desc; 

查询1990年出生的学生名单

select * from student where s_birth like "1990%";

查询英语成绩在60到80之间的

select * from score where s_score>60 and s_score<80 and c_id=(select c_id  from course where c_name='英语');
统计男女分别有多少个

select count(*) from student group by s_sex;

找出80后的学生

select * from student where s_birth like "198%";

-- 多表查询 

select * from stu.stu_ext  where stu.sid =stu_ext.sid;


select t2.father from stu t1,stu_ext t2 where t1.sid = t2.sid and t1.sname='小明';


结构 语法

-- inner jion


select * from stu t1 inner join  stu_ext t2 on t1.sid=t2.sid;

select * from stu t1 inner join stu_ext t2 on t1.sid=t2.sid inner join class t3 on t1.cid =t3.cid;

select t2.score from stu t1 inner join stu_ke t2 on t1.sid=t2.sid inner join ke t3 on t2.kid=t3.kid where t1.sname='小明' and t3.kname="英语";



select avg(t3.score),cname from stu t1 inner join class t2 on t1.cid=t2.cid inner join stu_ke t3 on t1.sid=t3.sid group by t2.cname;


SELECT
	avg(t3.score),
	cname
FROM
	stu t1
INNER JOIN class t2 ON t1.cid = t2.cid
INNER JOIN stu_ke t3 ON t1.sid = t3.sid
GROUP BY
	t2.cname;


select * from stu t1 inner stu t2 on t1.age=t2.age where t1.sname='小明';