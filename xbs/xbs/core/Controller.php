<?php

/**
 * 
 */
class Controller 
{
	public $data =[];
	public function display($template=null)
	{

		extract($this->data);

		if($template){
			include VIEW_PATH.'/'.strtolower(CONTROLLER_NAME).'/'.$template.'.html';
		}else{
			include VIEW_PATH.'/'.strtolower(CONTROLLER_NAME).'/'.ACTION_NAME.'.html';
		}
	}

	public function assign($key,$value)
	{
		$this->data[$key] = $value;
	}
}