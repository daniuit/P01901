<?php

// 定义在框架中需要使用的大量路径常量

define("COMMON_PATH",XBS_PATH.'/common');
define("CORE_PATH",XBS_PATH.'/core');
define("LIB_PATH",XBS_PATH.'/lib');
define("PUBLIC_PATH",'./public');
define("EXTENDS_PATH",'./extends');
define("STATIC_PATH",PUBLIC_PATH.'/static');

require COMMON_PATH.'/functions.php';



// var_dump($_SERVER['PATH_INFO']);

// exit;

// 解析路由

$module = isset($_GET['m'])?strtolower($_GET['m']):config('defalut_module');
$controller = isset($_GET['c'])? ucfirst(strtolower($_GET['c'])):config('defalut_controller');
$action = isset($_GET['a']) ? strtolower($_GET['a']):config('defalut_action');


// 定义模块 控制器 模型  视图 相关的常量

define('MODULE_PATH', APP_PATH.'/'.$module);

define('CONTROLLER_PATH', MODULE_PATH.'/controller');

define('MODEL_PATH', MODULE_PATH.'/model');

define('VIEW_PATH', MODULE_PATH.'/view');

define('MODULE_NAME',$module);

define('CONTROLLER_NAME',$controller);

define('ACTION_NAME',$action);


// var_dump(get_defined_constants(true)['user']);

// include CONTROLLER_PATH.'/'.$controller.'.php';
// 
// 自动加载类
function __autoload($classname)
{
	$paths = [
		CONTROLLER_PATH.'/'.$classname.'.php',
		MODEL_PATH.'/'.$classname.'.php',
		CORE_PATH.'/'.$classname.'.php',
		LIB_PATH.'/'.$classname.'.php',
		LIB_PATH.'/'.$classname.'.php',
	];

	foreach ($paths as  $path) {
		if(file_exists($path)){
			include $path;
			return;
		}
	}
}

// 实例化控制器
$obj = new $controller();
// 执行对应方法
if($action!=strtolower($controller)){
	$obj->$action();
}



